import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;

import javax.swing.JPanel;

/**
 * Ce composant dessine un carre qui subira des mises � l'�chelle
 * Notez que le carre est � l'origine, donc aucun deplacement
 * n'est produit par la mise a l'echelle
 *
 */

public class CarreScale extends JPanel {
	private static final long serialVersionUID = 1L;
	
	private final double COTE_CARRE = 55;
	private int facteurMiseEchelle = 1;
	private Rectangle2D.Double carre;

	/**
	 *  Creation du composant
	 */
	public CarreScale() {
		setBackground(Color.blue);
		carre = new Rectangle2D.Double( 0,  0,  COTE_CARRE, COTE_CARRE);
	}

	/**
	 * Dessiner le contenu du composant
	 * @param g Le contexte graphique du composant
	 */
	@Override
	public void paintComponent(Graphics g) {	
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D) g;
		
		g2d.setColor(Color.green);
		
		// scale le carre vert
		g2d.scale(facteurMiseEchelle, facteurMiseEchelle);
		
		g2d.fill(carre);	
	
	}//fin paintComponent

	/**
	 * Applique un facteur de mise � l'echelle au carre
	 * @param facteur Facteur de mise a l'echelle 
	 */
	public void setMiseEchelle(int facteur) {
		this.facteurMiseEchelle = facteur;
		repaint();
	}
	

}//fin classe
