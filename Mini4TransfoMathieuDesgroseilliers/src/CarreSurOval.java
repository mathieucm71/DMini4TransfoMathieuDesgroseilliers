import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;
import javax.swing.JPanel;

/**
 * Ce composant dessine un carre sur un oval, en utilisant une rotation.
 */

public class CarreSurOval extends JPanel {
	private static final long serialVersionUID = 1L;
	
	// constantes: dimensions des formes
	private final double DIAM_HORIZ= 78;
	private final double DIAM_VERT = 148;
	private final double COTE_CARRE = 50; 

	/**
	 *  Creation du composant
	 */
	public CarreSurOval() {
		setBackground(Color.gray);
	}

	/**
	 * Dessiner le contenu du composant
	 * @param g Le contexte graphique du composant
	 */
	@Override
	public void paintComponent(Graphics g) {
		Ellipse2D.Double oval;
		Rectangle2D.Double carre;
		double posX, posY;
		
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D) g;

		// etat initial avant rotation
		AffineTransform mat = g2d.getTransform();
		
		// rotation du cercle
		g2d.rotate(Math.PI/4, getWidth()/2, getHeight()/2);
		
		// ovale
		g2d.setColor( Color.magenta);
		posX = getWidth()/2.0-DIAM_HORIZ/2;
		posY = getHeight()/2-DIAM_VERT /2;
		oval = new Ellipse2D.Double (posX, posY, DIAM_HORIZ, DIAM_VERT );
		g2d.fill(oval);
		
		// retour origine rotation du cercle
		//g2d.rotate(-Math.PI/4, getWidth()/2, getHeight()/2);
		g2d.setTransform(mat);
		// carre rectangle
		g2d.setColor(Color.blue);
		posX = getWidth()/2.0-COTE_CARRE/2;
		posY = getHeight()/2.0-COTE_CARRE/2;
		carre = new Rectangle2D.Double( posX,  posY,  COTE_CARRE, COTE_CARRE);
		g2d.fill(carre);	
	
	}//fin paintComponent
	

}//fin classe
