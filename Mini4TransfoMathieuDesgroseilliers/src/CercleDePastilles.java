import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;

import javax.swing.JPanel;

/**
 * Ce composant dessine un carrre et des pastilles dessinees en cercle.
 */

public class CercleDePastilles extends JPanel {
	private static final long serialVersionUID = 1L;
	
	/* constantes � utiliser dans les calculs */
	private final double RAYON_PASTILLE = 9;  		//rayon des pastilles
	private final double RAYON_GROS_CERCLE = 79;   	//rayon du grand cercle sur lequel sont disposees les pastilles
	private final int NB_PASTILLES = 20;   			//nombre de pastilles a dessiner
	
	/**
	 *  Creation du composant
	 */
	public CercleDePastilles() {
		setBackground(Color.yellow);
	}

	/**
	 * Dessiner le contenu du composant
	 * @param g Le contexte graphique du composant
	 */
	@Override
	public void paintComponent(Graphics g) {
		Ellipse2D.Double pastille, pastilleDuCentre;
		double coinXPastille, coinYPastille;
		
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D) g;
		g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		
		double centreComposantX = getWidth()/2.0;
		double centreY= getHeight()/2.0;
		
		//on dessine le point rouge bien au centre
		coinXPastille = centreComposantX - RAYON_PASTILLE;
		coinYPastille = centreY - RAYON_PASTILLE;
		pastilleDuCentre  = new Ellipse2D.Double( coinXPastille, coinYPastille , RAYON_PASTILLE*2, RAYON_PASTILLE*2);
		g2d.setColor(Color.red);
		g2d.fill(pastilleDuCentre);
		
		//on dessine un carre dont le cote egale deux fois le rayon du gros cercle
		Rectangle2D.Double carre = new Rectangle2D.Double(centreComposantX-RAYON_GROS_CERCLE, centreY-RAYON_GROS_CERCLE, RAYON_GROS_CERCLE*2, RAYON_GROS_CERCLE*2 );
		g2d.draw(carre);
		
		//coin de la premiere pastille bleue
		coinXPastille = centreComposantX  + RAYON_GROS_CERCLE - RAYON_PASTILLE;
		pastille = new Ellipse2D.Double(coinXPastille, coinYPastille, RAYON_PASTILLE*2, RAYON_PASTILLE*2);
		
		g2d.setColor( new Color(100, 100, 200) ); 		
		
		for(int i = 0; i < 20; i++) {
			g2d.fill(pastille);
			g2d.rotate(Math.PI/10, centreComposantX, centreY);	
		}
	
		
	}//fin paintComponent
	

}//fin classe
