import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.event.ChangeListener;
import javax.swing.event.ChangeEvent;

/**
 * Cette application permet de tester diverses instructions de transformations
 * @author Mathieu Desgroseilliers
 * @author Caroline Houle
 *
 */

public class Application extends JFrame {
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;

	/**
	 * Demarrer l'application.
	 * @param args Param�tres d�entr�e de la commande de d�marrage
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Application frame = new Application();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Creer l'interface de l'application
	 * en y placant des instances des differents composants
	 */
	//par Caroline Houle
	public Application() {
		setTitle("Transformations");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 898, 285);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		CarreScale carreScale = new CarreScale();
		carreScale.setBounds(484, 11, 211, 197);
		contentPane.add(carreScale);
		
		CarreSurOval carreSurOval = new CarreSurOval();
		carreSurOval.setBounds(246, 11, 211, 197);
		contentPane.add(carreSurOval);
		
		CercleDePastilles cercleDePastilles = new CercleDePastilles();
		cercleDePastilles.setBounds(10, 11, 211, 197);
		contentPane.add(cercleDePastilles);
		
		JSpinner spinnerScaleCarreVert = new JSpinner();
		spinnerScaleCarreVert.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				
				carreScale.setMiseEchelle((int)(spinnerScaleCarreVert.getValue()));
				
			}
		});
		spinnerScaleCarreVert.setModel(new SpinnerNumberModel(1, 1, 3, 1));
		spinnerScaleCarreVert.setBounds(719, 135, 125, 73);
		contentPane.add(spinnerScaleCarreVert);
		
	}//fin constructeur
}//fin classe
